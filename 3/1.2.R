library(class)
library(stats)
library(caret)

set.seed(2345)

load('../idList-co-100.Rdata')
#id <- do.call(rbind, idList[1:10])
#id <- as.data.frame(id)
#id$V1 <- factor(id$V1)

id_train <- idList[1:4]
id_train <- as.data.frame(do.call(rbind, id_train))
id_train$V1 <- factor(id_train$V1)


#id_test <- idList[2:3]
#id_test <- as.data.frame(do.call(rbind, id_test))
#id_test$V1 <- factor(id_test$V1)

cluster = function(data, labels, size) {
  cipher_cluster <- c()
  label_cluster <- c()
  
  for (i in 0:9) {
    clusterData <- kmeans(data[labels == i, ], size)
    cipher_cluster[[i + 1]] <- clusterData$centers
    label_cluster[[i + 1]] <- c(1:size) * 0 + i
  }
  
  result <- list()
  result$data <- do.call(rbind, cipher_cluster)
  result$labels <- factor(unlist(label_cluster))
  
  return(result)
}


folds <- createFolds(id_train$V1, k = 10)
cluster50_result <- lapply(folds, function(x) {
  train_data <- id_train[x, -1]
  train_labels <- id_train[x, 1]
  
  test_data <- id_train[-x, -1]
  test_labels <- id_train[-x, 1]
  
  clusrters = cluster(train_data, train_labels, 50)
  
  before <- Sys.time()
  
  prediction <- knn(
    train = clusrters$data,
    test = test_data,
    cl = clusrters$labels,
    k = 3
  )
  
  result <- list()
  result$time <-
    as.double(difftime(Sys.time(), before, units = "secs"))
  matrix <- confusionMatrix(prediction, test_labels)
  result$accuracy <- matrix$overall[['Accuracy']]
  
  return(result)
})

folds <- createFolds(id_train$V1, k = 10)
cluster100_result <- lapply(folds, function(x) {
  train_data <- id_train[x, -1]
  train_labels <- id_train[x, 1]
  
  test_data <- id_train[-x, -1]
  test_labels <- id_train[-x, 1]
  
  clusrters = cluster(train_data, train_labels, 100)
  
  before <- Sys.time()
  
  prediction <- knn(
    train = clusrters$data,
    test = test_data,
    cl = clusrters$labels,
    k = 3
  )
  
  result <- list()
  result$time <-
    as.double(difftime(Sys.time(), before, units = "secs"))
  matrix <- confusionMatrix(prediction, test_labels)
  result$accuracy <- matrix$overall[['Accuracy']]
  
  return(result)
})

folds <- createFolds(id_train$V1, k = 10)
cluster150_result <- lapply(folds, function(x) {
  train_data <- id_train[x, -1]
  train_labels <- id_train[x, 1]
  
  test_data <- id_train[-x, -1]
  test_labels <- id_train[-x, 1]
  
  clusrters = cluster(train_data, train_labels, 150)
  
  before <- Sys.time()
  
  prediction <- knn(
    train = clusrters$data,
    test = test_data,
    cl = clusrters$labels,
    k = 3
  )
  
  result <- list()
  result$time <-
    as.double(difftime(Sys.time(), before, units = "secs"))
  matrix <- confusionMatrix(prediction, test_labels)
  result$accuracy <- matrix$overall[['Accuracy']]
  
  return(result)
})

folds <- createFolds(id_train$V1, k = 10)
no_cluster_result <- lapply(folds, function(x) {
  train_data <- id_train[x, -1]
  train_labels <- id_train[x, 1]
  
  test_data <- id_train[-x, -1]
  test_labels <- id_train[-x, 1]
  
  before <- Sys.time()
  
  prediction <- knn(
    train = train_data,
    test = test_data,
    cl = train_labels,
    k = 3
  )
  
  result <- list()
  result$time <-
    as.double(difftime(Sys.time(), before, units = "secs"))
  matrix <- confusionMatrix(prediction, test_labels)
  result$accuracy <- matrix$overall[['Accuracy']]
  
  return(result)
})

boxplot(
  unlist(lapply(cluster50_result, function(x)
    x[[2]])),
  unlist(lapply(cluster100_result, function(x)
    x[[2]])),
  unlist(lapply(cluster150_result, function(x)
    x[[2]])),
  unlist(lapply(no_cluster_result, function(x)
    x[[2]])),
  names = c("50","100","150" ,"No Cluster"),
  ylab = 'Accuracy',
  xlab = "Cluser Size"
)
title('Accuracy')

boxplot(
  unlist(lapply(cluster50_result, function(x)
    x[[1]])),
  unlist(lapply(cluster100_result, function(x)
    x[[1]])),
  unlist(lapply(cluster150_result, function(x)
    x[[1]])),
  unlist(lapply(no_cluster_result, function(x)
    x[[1]])),
  names = c("50","100","150" ,"No Cluster"),
  ylab = 'Time in Seconds',
  xlab = "Cluser Size"
)
title('Computational Time')
