library(class)
library(gmodels)
library(caret)
library(e1071)
source("util.R")

set.seed("123")

time_small = list()
acc_small = list()
time_large = list()
acc_large = list()
ks = list()

load("id100.Rda")
id_small <- id
index_small = split(id, 0.5)

dataset_train_small <- id_small[index_small, -1]
dataset_test_small <- id_small[-index_small, -1]

labels_train_small <- id_small[index_small, 1]
labels_test_small <- id_small[-index_small, 1]

load("idList-co-100.Rdata")
id_large <- do.call(rbind, idList[1:10])
id_large <- as.data.frame(id_large)
id_large$V1 <- factor(id_large$V1)

index_large = split(id_large, 0.5)

dataset_train_large <- id_large[index_large, -1]
dataset_test_large <- id_large[-index_large, -1]

labels_train_large  <- id_large [index_large , 1]
labels_test_large  <- id_large [-index_large , 1]

cs = c(1, 2, 4, 8, 16, 32, 64)
#cs = c(1,2)

for (k in cs) {
  before_small = Sys.time()
  
  prediction_small <- knn(
    train = dataset_train_small,
    test = dataset_test_small,
    cl = labels_train_small,
    k = k
  )
  
  time_small[k] <- difftime(Sys.time(), before_small, units = "secs")
  matrix <- confusionMatrix(prediction_small, labels_test_small)
  acc_small[k] <- matrix$overall[['Accuracy']]
  
  before_large = Sys.time()
  
  prediction_large <- knn(
    train = dataset_train_large,
    test = dataset_test_large,
    cl = labels_train_large,
    k = k
  )
  
  time_large[k] <-
    difftime(Sys.time(), before_large, units = "secs")
  matrix <- confusionMatrix(prediction_large, labels_test_large)
  acc_large[k] <- matrix$overall[['Accuracy']]
  
  ks[k] <- k
}

par(mar = c(4, 6, 4, 6) + 0.1)
plot(
  unlist(ks),
  unlist(acc_small),
  axes = FALSE,
  xlab = "",
  ylab = "",
  type = "b",
  ylim = c(0.94, 1)
)
axis(2, col = "black")
mtext("Accuracy", side = 2, line = 4)
par(new = TRUE)
plot(
  unlist(ks),
  unlist(time_small),
  axes = FALSE,
  xlab = "",
  ylab = "",
  col = "red",
  type = "b",
  ylim = c(0, 200)
)
axis(4, col = "red", col.axis = "red")
mtext("Time in Seconds" ,
      side = 4,
      col = "red",
      line = 4)
axis(1, pretty(range(ks), 10))
mtext("K",
      side = 1,
      col = "black",
      line = 2.5)
box(which = "plot")
title(main = "Samll Data Set")

par(mar = c(4, 6, 4, 6) + 0.1)
plot(
  unlist(ks),
  unlist(acc_large),
  axes = FALSE,
  xlab = "",
  ylab = "",
  type = "b",
  ylim = c(0.94, 1)
)
axis(2, col = "black")
mtext("Accuracy", side = 2, line = 4)
par(new = TRUE)
plot(
  unlist(ks),
  unlist(time_large),
  axes = FALSE,
  xlab = "",
  ylab = "",
  col = "red",
  type = "b",
  ylim = c(0, 200)
)
axis(4, col = "red", col.axis = "red")
mtext("Time in Seconds",
      side = 4,
      col = "red",
      line = 4)
axis(1, pretty(range(ks), 10))
mtext("K",
      side = 1,
      col = "black",
      line = 2.5)
box(which = "plot")
title(main = "Large Data Set")
