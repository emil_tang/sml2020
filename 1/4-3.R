library(class)
library(gmodels)
library(caret)
library(e1071)
source("util.R")


load("id100.Rda")

set.seed("123")

# 50 50 test train split

folds <- createFolds(id$X1, k = 10)

results <- lapply(folds, function(x) {
  dataset_train <- id[x, -1]
  dataset_test <- id[-x, -1]
  
  labels_train <- id[x, 1]
  labels_test <- id[-x, 1]
  
  before <- Sys.time()
  
  prediction <- knn(
    train = dataset_train,
    test = dataset_test,
    cl = labels_train,
    k = 3
  )
  after <- Sys.time()
  
  matrix <- confusionMatrix(prediction, labels_test)
  
  accuracy  <-  matrix$overall[['Accuracy']]
  time <- after - before
  
  return(list(time, accuracy))
})

times <- unlist(lapply(results, function(x)
  x[[1]]))
accuracies <- unlist(lapply(results, function(x)
  x[[2]]))

paste('accuracy:', 'mean:', mean(accuracies), 'sd:', sd(accuracies))
paste('time:', 'mean:', mean(times), 'sd:', sd(times))
