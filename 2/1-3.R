library(class)
library(caret)
source("../util.R")
set.seed(321)

load("../idList-co-100.Rdata")
id <- do.call(rbind, idList[1:10])
id <- as.data.frame(id)
id$V1 <- factor(id$V1)

index <- split(id, 0.5)
#index = 1:(nrow(id) / 2 ) + 1

dataset_train <- id[index, -1]
dataset_test <- id[-index, -1]

labels_train <- id[index, 1]
labels_test <- id[-index, 1]

pca <- prcomp(dataset_train)

time <- list()
acc <- list()

for (i in c(2:50)) {
    before <- Sys.time()
    
    train <- pca$x[, 1:i]
    test <- predict(pca, dataset_test)[, 1:i]
    
    prediction <- knn(
        train = train,
        test = test,
        cl = labels_train,
        k = 3
    )
    
    time[i] <-
        as.double(difftime(Sys.time(), before, units = "secs"))
    matrix <- confusionMatrix(prediction, labels_test)
    
    acc[i] <- matrix$overall[["Accuracy"]]
}

par(mar = c(4, 6, 4, 6) + 0.1)
plot(unlist(time),
     axes = FALSE,
     xlab = "",
     ylab = "",
     type = "b",
     ylim = c(0,50)
    )
axis(2, col = "black")
mtext("Time in Seconds", side = 2, line = 4)
par(new = TRUE)
plot(unlist(acc),
     axes = FALSE,
     xlab = "",
     ylab = "",
     col = "red",
     type = "b",
     ylim = c(0,1)
)
axis(4, col = "red", col.axis = "red")
mtext("Accuracy",
      col = "red",
      side = 4,
      line = 4)
axis(1)
mtext("Number of Principal Components",
      side = 1,
      line = 2.5)
box(which = "plot")
title('All Persons')
